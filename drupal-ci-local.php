<?php

require 'vendor/autoload.php';

use Symfony\Component\Yaml\Yaml;

$ci_file_array = Yaml::parseFile(__DIR__ . '/.gitlab-ci.yml');


$parent_keys = array_keys($ci_file_array);
// take all active jobs
$jobs_array = array_filter($ci_file_array, static function ($key) {
  return str_starts_with($key, 'PHP');
}, ARRAY_FILTER_USE_KEY);

foreach ($jobs_array as $name => $job) {

  $job_variables = [];

  //get the child jobs.
  $child_pipeline_array = Yaml::parseFile(__DIR__ . '/.gitlab-ci/pipeline.yml');
  $array_keys = array_keys($child_pipeline_array);
  foreach ($array_keys as $key => $array_key) {
    if (str_starts_with($array_key, '.')) {
      unset($child_pipeline_array[$array_key]);
    }
  }
  // merge job variables.
  if (empty($job['variables'])) {
    $job['variables'] = [];
  }
  if (empty($child_pipeline_array['variables'])) {
    $child_pipeline_array['variables'] = [];
  }
  $job_variables = array_merge($job['variables'], $child_pipeline_array['variables']);

  //get default pipeline parameters.
  $default_pipeline_parameters = $child_pipeline_array['default'] ?? [];
  unset($child_pipeline_array['stages'], $child_pipeline_array['include'], $child_pipeline_array['default'], $child_pipeline_array['variables']);
  // the keys of the jobs that need to be inserted to the new .gitlab-ci.yml is here, meaning, anything that doesn't start with '.' (snippets) or other gitlab ci/cd stuff
  $child_jobs = array_keys($child_pipeline_array);

  foreach ($child_jobs as $child_job_key) {
    if (!empty($job_variables)) {
      $child_pipeline_array[$child_job_key]['variables'] = array_merge($child_pipeline_array[$child_job_key]['variables'] ?? [], $job_variables);
    }
    if (isset($child_pipeline_array[$child_job_key]['variables']['CONCURRENCY']) && $child_pipeline_array[$child_job_key]['variables']['CONCURRENCY'] === '$CONCURRENCY') {
      //set the global concurrency variable to all jobs that refer the global variable
      $child_pipeline_array[$child_job_key]['variables']['CONCURRENCY'] = $ci_file_array['variables']['CONCURRENCY'];
    }
    //unset rules
    unset($child_pipeline_array[$child_job_key]['needs']);
  }
  //unset the old jobs from both arrays, we got what we need
  unset($jobs_array[$name], $ci_file_array[$name]);
  foreach (array_keys($child_pipeline_array) as $job_name) {
    $new_job_name = stripEmojis($job_name);
    $child_pipeline_array[$name . ' | ' . $new_job_name] = $child_pipeline_array[$job_name];
    unset($child_pipeline_array[$job_name]);
  }

  // merge default parameters to each test job.
  if (!empty($default_pipeline_parameters)) {
    foreach ($default_pipeline_parameters as $key => $parameter) {
      foreach ($child_pipeline_array as &$child_test_job) {
        if (!isset($child_test_job[$key])) {
          $child_test_job[$key] = $parameter;
        }
      }
    }
  }

  // add the new jobs to the main array
  $jobs_array += $child_pipeline_array;
}

// unset [Commit] and [Daily] jobs
$ci_file_array = array_filter($ci_file_array, static function($v){
  return !(str_starts_with($v, '[Commit]') || str_starts_with($v, '[Daily]'));
}, ARRAY_FILTER_USE_KEY);

//merge new jobs with the main array.
$ci_file_array += $jobs_array;
$ci_file_array['variables']['COMPOSER_ROOT_VERSION'] = '$CORE_MINOR';

$extended_gitlab_ci_file_contents = Yaml::dump($ci_file_array);

file_put_contents('.gitlab-ci--extended.yml', $extended_gitlab_ci_file_contents);

$local_file_env = "FILE=.gitlab-ci--extended.yml" . PHP_EOL . "CI_PIPELINE_SOURCE=parent_pipeline" . PHP_EOL;

file_put_contents('.gitlab-ci-local-env', $local_file_env);

echo "run 'drupal-ci-local --list-all' to see the new jobs" . PHP_EOL;

function stripEmojis($string) {
  //@src https://www.hashbangcode.com/snippets/php-function-strip-emojis-string
  // Convert question marks to a special thing so that we can remove
  // question marks later without any problems.
  $string = str_replace("?", "{%}", $string);
  // Convert the text into UTF-8.
  $string = mb_convert_encoding($string, "ISO-8859-1", "UTF-8");
  // Convert the text to ASCII.
  $string = mb_convert_encoding($string, "UTF-8", "ISO-8859-1");
  // Replace anything that is a question mark (left over from the conversion.
  $string = preg_replace('/(\s?\?\s?)/', ' ', $string);
  // Put back the .
  $string = str_replace("{%}", "?", $string);
  // Trim and return.
  return trim($string);
}
