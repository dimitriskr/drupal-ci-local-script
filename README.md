This file merges the files ".gitlab-ci.yml" and ".gitlab-ci/pipeline.yml" in core repositories so that firecow/gitlab-ci-local can run all the child pipelines

It requires a composer environment, with "symfony/yaml" present (already required by Drupal core) and PHP 8.1

# Download

```
wget https://gitlab.com/dimitriskr/drupal-ci-local-script/-/raw/main/drupal-ci-local.php
```

# Usage


* In order to generate the merged config, run `php script.php`

* In order to run the local CI, run `drupal-ci-local`

* List all the jobs generated by the script `drupal-ci-local --list-all`

By using `.gitlab-ci-local-env` we can set `drupal-ci-local` to use our custom file with some specific variables



## Drupal 7

In case of Drupal 7, you have to require the package `symfony/yaml` inside the D7 folder and then follow the steps above.

For example:
```
(php composer.phar|composer) require symfony/yaml
```