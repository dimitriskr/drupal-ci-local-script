<?php

require 'vendor/autoload.php';

$client = new GuzzleHttp\Client();

$api_url = "https://www.drupal.org/api-d7/node.json";
$params = [
  'headers' => [
    'Accept' => 'application/json'
  ],
  'query' => [
    'type' => 'project_issue',
    'field_issue_status' => 7,
    'field_project' => 3060,
  ]
];
$node_params  = [
  'headers' => [
    'Accept' => 'application/json'
  ],
  'query' => [
    'related_mrs' => 1,
  ]
];
$nids = [];
$req = $client->get($api_url, $params);
$v = json_decode($req->getBody()->getContents(), TRUE);


foreach (explode('&', $v['last']) as $chunk) {
  $param = explode("=", $chunk);
  if ($param) {
    if ($param[0] === 'page') {
      $last_page = (int) $param[1];
      break;
    }
  }
}
$opened = 0;
for ($i = $last_page; $i >= 990; $i--) {
  $params['query']['page'] = $i;

  $response = json_decode($client->get($api_url, $params)->getBody()->getContents(), TRUE);
  echo 'Accessing '.$response['self'] . PHP_EOL;
  foreach ($response['list'] as $issue) {
    $nids[] = $issue['nid'];
    $response = json_decode($client->get("https://www.drupal.org/api-d7/node/{$issue['nid']}.json", $node_params)->getBody()->getContents(), TRUE);
    $mrs = $response['related_mrs'] ?? [];
    if (!empty($mrs)) {
      foreach ($mrs as $mr_link) {
        $mr_parts = explode('/', $mr_link);
        $mr_id = $mr_parts[(count($mr_parts) - 1)];
        $gitlab_request = json_decode($client->get("https://git.drupalcode.org/api/v4/projects/project%2Fdrupal/merge_requests/$mr_id")->getBody()->getContents(), TRUE);
        if ($gitlab_request['state'] === 'opened') {
          echo $mr_link . PHP_EOL;
          $opened++;
          //uncomment to close MR. not tested
//          $client->put("https://git.drupalcode.org/api/v4/projects/project%2Fdrupal/merge_requests/$mr_id?state_event=close", ['headers' => ["PRIVATE-TOKEN" => '<your_access_token>'] ]);
        }
      }
    }
  }
}
echo "$opened open MRs that should be closed" . PHP_EOL;
